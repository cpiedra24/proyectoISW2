using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProyectoProMvc.Models;

namespace ProyectoProMvc.Controllers
{
    public class SoporteController : Controller
    {
        private readonly DataMvc _context;

        public SoporteController(DataMvc context)
        {
            _context = context;
        }

        // GET: Soporte
        public async Task<IActionResult> Index()
        {
            var dataMvc = _context.soporte.Include(s => s.cliente);
            return View(await dataMvc.ToListAsync());
        }

        // GET: Soporte/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var soporte = await _context.soporte
                .Include(s => s.cliente)
                .SingleOrDefaultAsync(m => m.ID == id);
            if (soporte == null)
            {
                return NotFound();
            }

            return View(soporte);
        }

        // GET: Soporte/Create
        public IActionResult Create()
        {
            ViewData["ClienteId"] = new SelectList(_context.cliente, "ID", "nombre");
            return View();
        }

        // POST: Soporte/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,TituloProblema,Detalle,QuienReporta,Estado,ClienteId")] soporte soporte)
        {
            if (ModelState.IsValid)
            {
                _context.Add(soporte);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClienteId"] = new SelectList(_context.cliente, "ID", "ID", soporte.ClienteId);
            return View(soporte);
        }

        // GET: Soporte/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var soporte = await _context.soporte.SingleOrDefaultAsync(m => m.ID == id);
            if (soporte == null)
            {
                return NotFound();
            }
            ViewData["ClienteId"] = new SelectList(_context.cliente, "ID", "ID", soporte.ClienteId);
            return View(soporte);
        }

        // POST: Soporte/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,TituloProblema,Detalle,QuienReporta,Estado,ClienteId")] soporte soporte)
        {
            if (id != soporte.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(soporte);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!soporteExists(soporte.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClienteId"] = new SelectList(_context.cliente, "ID", "ID", soporte.ClienteId);
            return View(soporte);
        }

        // GET: Soporte/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var soporte = await _context.soporte
                .Include(s => s.cliente)
                .SingleOrDefaultAsync(m => m.ID == id);
            if (soporte == null)
            {
                return NotFound();
            }

            return View(soporte);
        }

        // POST: Soporte/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var soporte = await _context.soporte.SingleOrDefaultAsync(m => m.ID == id);
            _context.soporte.Remove(soporte);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool soporteExists(int id)
        {
            return _context.soporte.Any(e => e.ID == id);
        }
    }
}
