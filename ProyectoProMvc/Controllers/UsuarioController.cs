using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProyectoProMvc.Models;

namespace ProyectoProMvc.Controllers
{
    public class UsuarioController : Controller
    {
        private readonly DataMvc _context;

        public UsuarioController(DataMvc context)
        {
            _context = context;
        }

        // GET: Usuario
        public async Task<IActionResult> Index()
        {
            return View(await _context.usuario.ToListAsync());
        }

        // GET: Usuario/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var usuario = await _context.usuario
                .SingleOrDefaultAsync(m => m.ID == id);
            if (usuario == null)
            {
                return NotFound();
            }

            return View(usuario);
        }

        // GET: Usuario/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Usuario/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,nombre,apellido,usuarios,contrasena,tipoUsuario")] usuario usuario)
        {
            if (ModelState.IsValid)
            {
                _context.Add(usuario);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(usuario);
        }

        // GET: Usuario/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var usuario = await _context.usuario.SingleOrDefaultAsync(m => m.ID == id);
            if (usuario == null)
            {
                return NotFound();
            }
            return View(usuario);
        }

        // POST: Usuario/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,nombre,apellido,usuarios,contrasena,tipoUsuario")] usuario usuario)
        {
            if (id != usuario.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(usuario);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!usuarioExists(usuario.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(usuario);
        }

        // GET: Usuario/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var usuario = await _context.usuario
                .SingleOrDefaultAsync(m => m.ID == id);
            if (usuario == null)
            {
                return NotFound();
            }

            return View(usuario);
        }

        // POST: Usuario/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var usuario = await _context.usuario.SingleOrDefaultAsync(m => m.ID == id);
            _context.usuario.Remove(usuario);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool usuarioExists(int id)
        {
            return _context.usuario.Any(e => e.ID == id);
        }
        public IActionResult Login()
        {
            return View();
        }
        //Metodo para logear, se obtienen los datos de la base de datos y se asignan a la variable usua, se compara
        //usuario con usuario y contra con contra y despues se pregunta si la variable usua viene null de no ser asi
        //se crea la varaiable session donde se le hace el ser del usuario y se compara el tipo de usuario..
        [HttpPost]
        public async Task<IActionResult> Login([Bind("usuarios,contrasena,tipoUsuario")] usuario usuario)
        {
            var usua = await _context.usuario
               .SingleOrDefaultAsync(u => u.usuarios == usuario.usuarios && u.contrasena == usuario.contrasena);

            if (usua != null)
            {
                HttpContext.Session.SetString("usuarios", usua.ToString());
                if (usua.tipoUsuario == "admin")
                {
                    //Se redirecciona al public Admin para hacer el llamado a la vista
                    return RedirectToAction("Admin");
                }
                else if (usua.tipoUsuario == "usuario")
                {
                    //Se redirecciona al public UsuarioCorriente para hacer el llamado a la vista
                    return RedirectToAction("UsuarioCorriente");
                }
            }
            else
            {
                ModelState.AddModelError("", "El usuario o contra incorrectas");
                RedirectToAction("Index");
            }
            return View();
        }

        //metodo para llamar la interfaz de UsuarioCorriente, a la ves se le asigna el 
        // mediante el ViewBag  el tipo de usuario para cuando llegue a la vista hacer validaciones.
        public IActionResult UsuarioCorriente()
        {
            ViewBag.tipo = "usuario";
            return View();
        }
        //metodo para llamar la interfaz de usuario Admin, a la ves se le asigna el 
        // mediante el ViewBag  el tipo de usuario para cuando llegue a la vista hacer validaciones.
        public IActionResult Admin()
        {
            ViewBag.usu = "admin";
            return View();
        }
    }
}
