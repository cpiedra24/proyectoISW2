﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ProyectoProMvc.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "cliente",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    PgWeb = table.Column<string>(nullable: true),
                    cedulaJuridica = table.Column<string>(nullable: true),
                    direccionFisica = table.Column<string>(nullable: true),
                    nombre = table.Column<string>(nullable: true),
                    sector = table.Column<string>(nullable: true),
                    telefono = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_cliente", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "usuario",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    apellido = table.Column<string>(nullable: true),
                    contrasena = table.Column<string>(nullable: true),
                    nombre = table.Column<string>(nullable: true),
                    tipoUsuario = table.Column<string>(nullable: true),
                    usuarios = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_usuario", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "contacto",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ClienteId = table.Column<int>(nullable: false),
                    apellido = table.Column<string>(nullable: true),
                    correo = table.Column<string>(nullable: true),
                    nombre = table.Column<string>(nullable: true),
                    numero = table.Column<int>(nullable: false),
                    puesto = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_contacto", x => x.ID);
                    table.ForeignKey(
                        name: "FK_contacto_cliente_ClienteId",
                        column: x => x.ClienteId,
                        principalTable: "cliente",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "soporte",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ClienteId = table.Column<int>(nullable: false),
                    Detalle = table.Column<string>(nullable: true),
                    Estado = table.Column<string>(nullable: true),
                    QuienReporta = table.Column<string>(nullable: true),
                    TituloProblema = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_soporte", x => x.ID);
                    table.ForeignKey(
                        name: "FK_soporte_cliente_ClienteId",
                        column: x => x.ClienteId,
                        principalTable: "cliente",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "reunion",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UsuarioId = table.Column<int>(nullable: false),
                    fecha = table.Column<DateTime>(nullable: false),
                    tituloReunion = table.Column<string>(nullable: true),
                    virtuals = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_reunion", x => x.ID);
                    table.ForeignKey(
                        name: "FK_reunion_usuario_UsuarioId",
                        column: x => x.UsuarioId,
                        principalTable: "usuario",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_contacto_ClienteId",
                table: "contacto",
                column: "ClienteId");

            migrationBuilder.CreateIndex(
                name: "IX_reunion_UsuarioId",
                table: "reunion",
                column: "UsuarioId");

            migrationBuilder.CreateIndex(
                name: "IX_soporte_ClienteId",
                table: "soporte",
                column: "ClienteId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "contacto");

            migrationBuilder.DropTable(
                name: "reunion");

            migrationBuilder.DropTable(
                name: "soporte");

            migrationBuilder.DropTable(
                name: "usuario");

            migrationBuilder.DropTable(
                name: "cliente");
        }
    }
}
