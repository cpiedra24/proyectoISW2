﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using ProyectoProMvc.Models;
using System;

namespace ProyectoProMvc.Migrations
{
    [DbContext(typeof(DataMvc))]
    partial class DataMvcModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.1-rtm-125");

            modelBuilder.Entity("ProyectoProMvc.Models.cliente", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("PgWeb");

                    b.Property<string>("cedulaJuridica");

                    b.Property<string>("direccionFisica");

                    b.Property<string>("nombre");

                    b.Property<string>("sector");

                    b.Property<string>("telefono");

                    b.HasKey("ID");

                    b.ToTable("cliente");
                });

            modelBuilder.Entity("ProyectoProMvc.Models.contacto", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ClienteId");

                    b.Property<string>("apellido");

                    b.Property<string>("correo");

                    b.Property<string>("nombre");

                    b.Property<int>("numero");

                    b.Property<string>("puesto");

                    b.HasKey("ID");

                    b.HasIndex("ClienteId");

                    b.ToTable("contacto");
                });

            modelBuilder.Entity("ProyectoProMvc.Models.reunion", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("UsuarioId");

                    b.Property<DateTime>("fecha");

                    b.Property<string>("tituloReunion");

                    b.Property<bool>("virtuals");

                    b.HasKey("ID");

                    b.HasIndex("UsuarioId");

                    b.ToTable("reunion");
                });

            modelBuilder.Entity("ProyectoProMvc.Models.soporte", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ClienteId");

                    b.Property<string>("Detalle");

                    b.Property<string>("Estado");

                    b.Property<string>("QuienReporta");

                    b.Property<string>("TituloProblema");

                    b.HasKey("ID");

                    b.HasIndex("ClienteId");

                    b.ToTable("soporte");
                });

            modelBuilder.Entity("ProyectoProMvc.Models.usuario", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("apellido");

                    b.Property<string>("contrasena");

                    b.Property<string>("nombre");

                    b.Property<string>("tipoUsuario");

                    b.Property<string>("usuarios");

                    b.HasKey("ID");

                    b.ToTable("usuario");
                });

            modelBuilder.Entity("ProyectoProMvc.Models.contacto", b =>
                {
                    b.HasOne("ProyectoProMvc.Models.cliente", "cliente")
                        .WithMany()
                        .HasForeignKey("ClienteId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ProyectoProMvc.Models.reunion", b =>
                {
                    b.HasOne("ProyectoProMvc.Models.usuario", "usuario")
                        .WithMany()
                        .HasForeignKey("UsuarioId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ProyectoProMvc.Models.soporte", b =>
                {
                    b.HasOne("ProyectoProMvc.Models.cliente", "cliente")
                        .WithMany()
                        .HasForeignKey("ClienteId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
