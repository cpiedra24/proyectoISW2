using System;

namespace ProyectoProMvc.Models
{
    public class cliente
    {
        public int ID { get; set; }
        public string nombre { get; set; }
        public string cedulaJuridica { get; set; }
        public string PgWeb { get; set; }
        public string direccionFisica { get; set; }
        public string telefono { get; set; }
        public string sector { get; set;} 
    }
}