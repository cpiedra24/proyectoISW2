using System;


namespace ProyectoProMvc.Models
{
    public class contacto
    {
        public int ID { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string correo { get; set; }
        public string puesto { get; set; }
        public int numero { get; set; }
        public int ClienteId { get; set; }
        public virtual cliente cliente { get; set; }

    }
}