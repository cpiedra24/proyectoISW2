using Microsoft.EntityFrameworkCore;

namespace ProyectoProMvc.Models
{
    public class DataMvc : DbContext
    {
        public DataMvc(DbContextOptions<DataMvc> options)
            : base(options)
        {
        }
        public DbSet<cliente> cliente { get; set; }
        public DbSet<usuario> usuario { get; set; }
        public DbSet<contacto> contacto { get; set; }
        public DbSet<reunion> reunion { get; set; }
        public DbSet<soporte> soporte { get; set; }
    }
}