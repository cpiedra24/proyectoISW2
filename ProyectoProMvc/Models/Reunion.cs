using System;
using System.ComponentModel.DataAnnotations;

namespace ProyectoProMvc.Models
{
    public class reunion
    {
        public int ID { get; set; }
        public string tituloReunion { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy}", ApplyFormatInEditMode = true)]
        public DateTime fecha { get; set; }
        public Boolean virtuals { get; set; }
        public int UsuarioId { get; set; }
        public virtual usuario usuario { get; set; }

    }
}