using System;

namespace ProyectoProMvc.Models
{
    public class soporte
    {
        public int ID { get; set; }
        public string TituloProblema { get; set; }
        public string Detalle { get; set; }
        public string QuienReporta { get; set; }
        public string Estado { get; set; }
        public int ClienteId { get; set; }
        public virtual cliente cliente { get; set; }

    }
}