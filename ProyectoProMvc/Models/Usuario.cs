using System;

namespace ProyectoProMvc.Models
{
    public class usuario
    {
        public int ID { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string usuarios { get; set; }
        public string contrasena { get; set; }
        public string tipoUsuario { get; set; }
    }
}